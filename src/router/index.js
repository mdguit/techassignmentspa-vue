import Vue from 'vue'
import Router from 'vue-router'
import ContactList from '@/components/ContactList'
import Details from '@/components/Details'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: ContactList
    },
    {
      path: '/details/:id',
      name: 'details',
      component: Details,
      props: true
    }
  ]
})
