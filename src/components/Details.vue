<template>
  <div>
    <div style="display: flex">
      <img style="padding-right: 20px" id="backIcon" @click="$router.go(-1)" src="../assets/back-arrow.svg" width="35">
      <h3>Детали контакта</h3>
    </div>
    <form>
      <div id="firstNameBlock">
        <label for="fname">Имя</label>
        <div class="inputAndAction">
          <input type="text" id="fname" name="firstname" v-bind:value="firstName" v-on:change="saveToStateHistory($event.target.value, 'inputValues', 'firstName')" placeholder="Вставьте имя..">
          <img class="actionIcons" @click="openDeleteFieldModal('firstNameBlock')" src="../assets/trash-can.svg" width="35">
          <img class="actionIcons" @click="openCancelChangesFieldModal('fname')"  src="../assets/cancel.svg" width="35">
        </div>
      </div>

      <div id="lastNameBlock">
        <label for="lname">Фамилия</label>
        <div class="inputAndAction">
          <input type="text" v-bind:value="lastName" v-on:change="saveToStateHistory($event.target.value, 'inputValues', 'lastName')" id="lname" name="lastname" placeholder="Вставьте фамилию..">
          <img class="actionIcons" @click="openDeleteFieldModal('lastNameBlock')" src="../assets/trash-can.svg" width="35">
          <img class="actionIcons"  @click="openCancelChangesFieldModal('lname')" src="../assets/cancel.svg" width="35">
        </div>
      </div>

      <div id="emailIdBlock">
        <label for="eMail">Электронная почта</label>
        <div class="inputAndAction">
          <input type="text" v-bind:value="email" id="eMail" name="eMail" v-on:change="saveToStateHistory($event.target.value, 'inputValues', 'email')" placeholder="Вставьте адрес электронной почты..">
          <img class="actionIcons" @click="openDeleteFieldModal('emailIdBlock')" src="../assets/trash-can.svg" width="35">
          <img class="actionIcons"  @click="openCancelChangesFieldModal('eMail')" src="../assets/cancel.svg" width="35">
        </div>
      </div>

      <div id="phoneNumberBlock">
        <label for="pnumber">Номер телефона</label>
        <div class="inputAndAction">
          <input type="text" v-bind:value="phoneNumber" id="pnumber" name="pnumber"  v-on:change="saveToStateHistory($event.target.value, 'inputValues', 'phoneNumber')" placeholder="Вставьте номер телефона..">
          <img class="actionIcons" @click="openDeleteFieldModal('phoneNumberBlock')" src="../assets/trash-can.svg" width="35">
          <img class="actionIcons" @click="openCancelChangesFieldModal('pnumber')" src="../assets/cancel.svg" width="35">
        </div>
      </div>

      <div>
        <img @click="addInputBlock()" id="plusIcon" style="margin-bottom: 20px;" src="../assets/plus.svg" width="35">
        <img @click="stepBackToPreviousState()" id="stepBackIcon" style="margin-bottom: 20px;" src="../assets/step-back-button.svg" width="35">
      </div>

      <input type="submit" value="Отправить" @click="editData()">
    </form>

    <div id="delete_field" class="modal">
      <div class="modal-content">
        <span @click="closeDeleteFieldModal" class="close">&times;</span>
        <p>Вы действительно хотите удалить это поле?</p>
        <button @click="deleteInputBlock(deleteId)">Да</button>
        <button @click="closeDeleteFieldModal">Нет</button>
      </div>
    </div>

    <div id="cancel_field" class="modal">
      <div class="modal-content">
        <span @click="closeCancelChangesFieldModal" class="close">&times;</span>
        <p>Вы действительно хотите отменить все изменения этого поля?</p>
        <button @click="cancelChangesInTheField(cancelId)">Да</button>
        <button @click="closeCancelChangesFieldModal">Нет</button>
      </div>
    </div>

  </div>
</template>

<script>
import router from '../router'

export default {
  props: ['id'], // получает id текущего контакта из рута
  name: 'Details',
  data () {
    return {
      contacts: [],  // массив данных о контактах, используется только для взятия информации о текущем контакте
      contact: {}, // объект с информацией о текущем контакте
      stateHistory: [], // массив с историей состояний, для возможности исправлять последнее сделанное изменение
      firstName: '', // аттрибуты контакта, для примера я взял имя, фамилию, электронную почту и телефон
      lastName: '',
      email: '',
      phoneNumber: '',
      deleteId: '', // id DOM, которое должно быть удалено
      cancelId: '' // id DOM, в котором нужно убрать изменения
    }
  },
  created () {
    this.contacts = JSON.parse(localStorage.getItem('contactInfo'))
    this.contact = Array.from(this.contacts).find(x => x.id === +this.id)

    if (this.contact) {  // инициализация начальных значений аттрибутов
      this.firstName = this.contact.firstName
      this.lastName = this.contact.lastName
      this.email = this.contact.email
      this.phoneNumber = this.contact.phoneNumber
    }
  },
  mounted () { // определение, какие поля должны быть доступны сразу для редактирования, а какие только после добавления
    let isFirstNamePresented,
      isLastNamePresented,
      isEmailPresented,
      isPhoneNumberPresented

    isFirstNamePresented = !(this.firstName === null || this.firstName === undefined || this.firstName === '')
    isLastNamePresented = !(this.lastName === null || this.lastName === undefined || this.lastName === '')
    isEmailPresented = !(this.email === null || this.email === undefined || this.email === '')
    isPhoneNumberPresented = !(this.phoneNumber === null || this.phoneNumber === undefined || this.phoneNumber === '')

    this.checkIfBlockShouldBePopped(isFirstNamePresented, isLastNamePresented, isEmailPresented, isPhoneNumberPresented)
    this.stateHistory = [  // изначальный массив с историей состояний, то есть, начальный стейт
      {inputValues: this.contact,
        displayValues:
          {isFirstNamePresented: isFirstNamePresented,
            isLastNamePresented: isLastNamePresented,
            isEmailPresented: isEmailPresented,
            isPhoneNumberPresented: isPhoneNumberPresented}
      }
    ]

    localStorage.setItem('stateHistory', JSON.stringify(this.stateHistory)) // добавления массива историй состояния в локальное хранилище
    this.stateHistory = []
  },
  methods: {
    // функция, которая возвращает предыдущее состояние, то есть отменяет последнее сделанное изменение
    stepBackToPreviousState () {
      let stateHistory = Array.from(JSON.parse(localStorage.getItem('stateHistory')))
      let previouseState;
      if (stateHistory.length <= 2) {
        previouseState = stateHistory[0]
      } else {
        previouseState = stateHistory[stateHistory.length - 2]
      }

      // здесь объявляются значения, которые были у полей до последнего изменения

      document.getElementById('fname').value = previouseState['inputValues']['firstName']
      document.getElementById('lname').value = previouseState['inputValues']['lastName']
      document.getElementById('eMail').value = previouseState['inputValues']['email']
      document.getElementById('pnumber').value = previouseState['inputValues']['phoneNumber']

      // вызывается метод, который удаляет поля, которых не существовало до последних изменений

      this.checkIfBlockShouldBePopped(previouseState['displayValues']['isFirstNamePresented'],
        previouseState['displayValues']['isLastNamePresented'],
        previouseState['displayValues']['isEmailPresented'],
        previouseState['displayValues']['isPhoneNumberPresented'])

      // вызывается метод, который добавляет поля, который были удалены после последнего изменения

      this.restoreDeletedBlocks(previouseState['displayValues']['isFirstNamePresented'],
        previouseState['displayValues']['isLastNamePresented'],
        previouseState['displayValues']['isEmailPresented'],
        previouseState['displayValues']['isPhoneNumberPresented'])

      // предотвращает полное уничтожения истории состояний

      if (stateHistory.length > 1) {
        stateHistory.pop();
      }
      localStorage.setItem('stateHistory', JSON.stringify(stateHistory));
    },

    // метод, который постоянно обновляет историю изменений

    saveToStateHistory (value, stateType, name) {
      let anotherTestArray = JSON.parse(localStorage.getItem('stateHistory'))
      const currentHistory = Array.from(JSON.parse(localStorage.getItem('stateHistory')))
      let newEntry = currentHistory[currentHistory.length - 1]
      newEntry[stateType][name] = value
      this.stateHistory = anotherTestArray
      this.stateHistory.push(newEntry)
      localStorage.setItem('stateHistory', JSON.stringify(this.stateHistory))
    },

    // закрывает модаль с подтверждением о удалении поля ввода

    closeDeleteFieldModal () {
      document.getElementById('delete_field').style.display = 'none'
      document.querySelector('body').classList.remove('overflow-hidden')
    },

    //  открывает модаль с подтверждением о удалении поля ввода

    openDeleteFieldModal (deleteId) {
      this.deleteId = deleteId
      document.getElementById('delete_field').style.display = 'block'
      document.querySelector('body').classList.add('overflow-hidden')
    },

    // закрывает модаль с подтверждением о отмене всех изменений с поля ввода

    closeCancelChangesFieldModal () {
      document.getElementById('cancel_field').style.display = 'none'
      document.querySelector('body').classList.remove('overflow-hidden')
    },

    // открывает модаль с подтверждением о отмене всех изменений с поля ввода

    openCancelChangesFieldModal (cancelId) {
      this.cancelId = cancelId
      document.getElementById('cancel_field').style.display = 'block'
      document.querySelector('body').classList.add('overflow-hidden')
    },

    // метод, который проверяет, какие поля не должны быть показаны пользователю

    checkIfBlockShouldBePopped (firstName, lastName, email, phoneNumber) {
      if (!firstName) {
        document.getElementById('firstNameBlock').style.display = 'none'
      }
      if (!lastName) {
        document.getElementById('lastNameBlock').style.display = 'none'
      }
      if (!email) {
        document.getElementById('emailIdBlock').style.display = 'none'
      }
      if (!phoneNumber) {
        document.getElementById('phoneNumberBlock').style.display = 'none'
      }
    },

    // метод, который возвращает удаленные поля при последнем изменении

    restoreDeletedBlocks (firstName, lastName, email, phoneNumber) {
      if (firstName) {
        document.getElementById('firstNameBlock').style.display = 'block'
      }
      if (lastName) {
        document.getElementById('lastNameBlock').style.display = 'block'
      }
      if (email) {
        document.getElementById('emailIdBlock').style.display = 'block'
      }
      if (phoneNumber) {
        document.getElementById('phoneNumberBlock').style.display = 'block'
      }
    },

    // метод, который добавляет поля, если они отсутствуют по нажатию на иконку "плюс"

    addInputBlock () {
      let firstNameBlock,
        lastNameBlock,
        emailIdBlock,
        phoneNumberBlock

      firstNameBlock = document.getElementById('firstNameBlock').style.display
      lastNameBlock = document.getElementById('lastNameBlock').style.display
      emailIdBlock = document.getElementById('emailIdBlock').style.display
      phoneNumberBlock = document.getElementById('phoneNumberBlock').style.display

      if (document.getElementById('firstNameBlock').style.display === 'none') {
        document.getElementById('firstNameBlock').style.display = 'block'
        this.saveToStateHistory(true, 'displayValues', 'isFirstNamePresented')
        return
      }

      if (document.getElementById('lastNameBlock').style.display === 'none') {
        document.getElementById('lastNameBlock').style.display = 'block'
        this.saveToStateHistory(true, 'displayValues', 'isLastNamePresented')
        return
      }

      if (document.getElementById('emailIdBlock').style.display === 'none') {
        document.getElementById('emailIdBlock').style.display = 'block'
        this.saveToStateHistory(true, 'displayValues', 'isEmailPresented')
        return
      }

      if (document.getElementById('phoneNumberBlock').style.display === 'none') {
        document.getElementById('phoneNumberBlock').style.display = 'block'
        this.saveToStateHistory(true, 'displayValues', 'isPhoneNumberPresented')
      }

      if (firstNameBlock !== 'none' &&
        lastNameBlock !== 'none' &&
        emailIdBlock !== 'none' &&
        phoneNumberBlock !== 'none') {
        alert('Вы достигли максимального количества полей для контакта')
      }
    },

    // метод, который удаляет ненужный блок

    deleteInputBlock (deleteId) {
      document.getElementById(deleteId).style.display = 'none'
      document.getElementById(deleteId).getElementsByTagName('div')[0].getElementsByTagName('input')[0].value = ''
      this.closeDeleteFieldModal()
      let name
      if (deleteId === 'firstNameBlock') {
        name = 'isFirstNamePresented'
      } else if (deleteId === 'lastNameBlock') {
        name = 'isLastNamePresented'
      } else if (deleteId === 'emailIdBlock') {
        name = 'isEmailPresented'
      } else if (deleteId === 'phoneNumberBlock') {
        name = 'isPhoneNumberPresented'
      }
      this.saveToStateHistory(false, 'displayValues', name)
    },

    // метод, который отменяет все изменения в поле

    cancelChangesInTheField (cancelId) {
      if (cancelId === 'fname') {
        document.getElementById(cancelId).value = this.firstName
      }

      if (cancelId === 'lname') {
        document.getElementById(cancelId).value = this.lastName
      }

      if (cancelId === 'eMail') {
        document.getElementById(cancelId).value = this.email
      }

      if (cancelId === 'phoneNumber') {
        document.getElementById(cancelId).value = this.phoneNumber
      }

      this.closeCancelChangesFieldModal()
    },

    // метод, симулирующий отправку новых данных

    editData () {
      let firstName,
        lastName,
        email,
        phoneNumber,
        newArray

      firstName = document.getElementById('fname').value
      lastName = document.getElementById('lname').value
      email = document.getElementById('eMail').value
      phoneNumber = document.getElementById('pnumber').value

      const id = this.contact.id

      newArray = Array.from(this.contacts).filter(x => x.id !== this.contact.id)
      newArray.push({id: id, firstName: firstName, lastName: lastName, email: email, phoneNumber: phoneNumber})

      localStorage.setItem('contactInfo', JSON.stringify(newArray))
      router.push({name: 'home'})
    }
  },
  beforeDestroy () {
    this.stateHistory = []
    localStorage.removeItem('stateHistory')

    // очищает историю изменений
  }

}
</script>

<style scoped>
  input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type=submit] {
    width: 100%;
    background-color: #bebcc1;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  input[type=submit]:hover {
    background-color: #bbbbbb;
  }

  div {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
  }

  .inputAndAction {
    display: flex;
  }

  .actionIcons {
    padding-left: 10px;
    padding-bottom: 5px;
  }

  .actionIcons:hover {
    cursor: pointer;
  }

  #plusIcon:hover {
    cursor: pointer;
  }

  #stepBackIcon:hover {
    cursor: pointer;
  }

  #backIcon:hover {
    cursor: pointer;
  }

  .modal {
    display: none;
    position: fixed;
    z-index: 1;
    padding-top: 100px;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
  }

  .modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 80%;
  }

  .close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }

  .close:hover,
  .close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
  }
</style>
